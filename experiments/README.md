# Tagada experiments

The Tagada experiments are run using the Whitesmith binary. It requires to have [https://doc.rust-lang.org/cargo/getting-started/installation.html](`cargo`) installed.

The whitesmith experiments need to perform 3 tasks.

1. Fetch. This task will download the sources requires to execute the experiments.
2. Build. This task will build the project.
3. Run this task will run the experiments.

## Example

```sh
whitesmith rijndael/search-rk-dc.ron fetch
```

```sh
whitesmith rijndael/search-rk-dc.ron build -o USER_UID:$(id -u) -o GROUP_UID:$(id -g)
```

```sh
whitesmith rijndael/search-rk-dc.ron run -o PWD:$(pwd) \
  --only 128-128-3.spec \
  --only 128-128-3.diff \
  --only 128-128-3.trunc \
  --only 128-128-3.search-best-dc 
```
