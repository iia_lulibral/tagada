#!/usr/bin/sh

TOOLS=/home/default/tagada/tagada-tools/target/release/tagada-tools
TRANSFORM=derive

# $1: The output filename

$TOOLS transform $TRANSFORM "$1" &
wait $!
