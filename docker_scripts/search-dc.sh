#!/usr/bin/bash

TOOLS=/home/default/tagada/tagada-tools/target/release/tagada-tools
SEARCH=best-differential-characteristic

# $1: The differential graph filename
# $2: The truncated graph filename

$TOOLS search $SEARCH --encoding cnf "$1" "$2" picat &
wait $!
