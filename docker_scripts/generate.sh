#!/usr/bin/bash

SPECS=/home/default/tagada/tagada-specs

# $1: Cipher name
# $2: Cipher building arguments

bundle exec "$SPECS/ciphers/$1.rb" "${@:2}" &
wait $!