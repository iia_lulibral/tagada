#!/usr/bin/bash

TOOLS=/home/default/tagada/tagada-tools/target/release/tagada-tools
TRANSFORM=truncate-differential

# $1: The output filename
# $2+: The truncation options

$TOOLS transform $TRANSFORM "${@:2}" "$1" & 
wait $! 
